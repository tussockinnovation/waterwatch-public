import { combineReducers } from 'redux';

import sensors from './sensors';
import measurements from './measurements';

export const rootReducer = combineReducers<AppState>({
  sensors,
  measurements,
});