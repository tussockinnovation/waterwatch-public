import axios from 'axios';
import config from '../config'

export default function callApi(resource: string) {
  return axios.get(`${config.apiUrl}/${resource}`, {
    auth: config.privateConfig.waterwatchApiAuth
  });
}