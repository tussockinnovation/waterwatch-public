interface AppState {
  sensors: Resource<Sensor[]>;
  measurements: Measurements;
}

interface Resource<T> {
  isLoaded: boolean;
  data: T;
}

interface Sensor {
  name: string;
  serial: string;
  latestData?: LatestSensorData;
  config?: SensorConfiguration;
  displayInfo?: SensorDisplayInfo;
}

interface SensorDisplayInfo {
  offsetMeasurement: number | null;
  minLevel: number | null;
  maxLevel: number | null;
}

interface LatestSensorData {
  alertAsserted: boolean;
  offlineAsserted?: boolean;
  battery?: number;
  lastSeen: string; //ISO date string
  lastMeasurement: Measurement;
  signalLevel: number; //signal level in dbm
  linkQuality?: string;
}

interface SensorConfiguration {
  measurementInterval: number; //number of minutes between measurements
  transmissionInterval: number; //number of minutes between transmits
  schedulerEnabled: boolean; //is the measurement scheduler enabled
  measurementTransmissionEnabled: boolean; //are measurements transmistted
  serverSideAlarm?: boolean;
  alarm: AlarmConfiguration;
}

interface AlarmConfiguration {
  threshold: number; //distance in mm
  type: ThresholdType;
}

declare const enum ThresholdType {
  INCREASING_DISTANCE = 0,
  DECREASING_DISTANCE = 1,
}

interface Measurements {
  [sensorSerial: string]: Measurement[];
}

interface Measurement {
  rawDistance: number;
  level: number;
  offset: number;
  time: number;
}

declare const enum ActionType {
  GET_SENSORS = 'GET_SENSORS',
  UPDATE_SENSORS = 'UPDATE_SENSORS',

  GET_MEASUREMENTS = 'GET_MEASUREMENTS',
  UPDATE_MEASUREMENTS = 'UPDATE_MEASUREMENTS',
}

declare namespace Actions {
  interface BaseAction<PayloadType> {
    type: string;
    payload: PayloadType;
  }

  // Sensors
  interface GetSensors extends BaseAction<null> { }
  interface UpdateSensors extends BaseAction<{ sensors: Sensor[]; overwrite: boolean; }> { }

  // Measurements
  interface GetMeasurements extends BaseAction<{ sensorSerial: string }> { }
  interface UpdateMeasurements extends BaseAction<{ measurements: Measurements; overwrite: boolean; }> { }
}